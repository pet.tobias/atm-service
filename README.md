# Bank Service

## Swagger UI:
```
http://localhost:8080/swagger-ui.html
```
### Workflow
```
1. Validate a card number
2. Authorize with the returned token
3. Validate with the returned auth method
4. "Logout" and authorize with the newly returned token
5. Use Account interfaces
```


## Entities
### CardHolder 1
```
CardNumber: 0000000000000000
PIN: 0000
FingerPrint: finger0
```
### CardHolder 2
```
CardNumber: 1111111111111111
PIN: 1111
FingerPrint: finger1
```
### CardHolder 3
```
CardNumber: 2222222222222222
PIN: 2222
FingerPrint: finger2
```
