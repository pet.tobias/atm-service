package com.tobias.atmservice.security;

import com.tobias.atmservice.dto.AuthMethod;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TokenProviderTest {


    @InjectMocks
    private TokenProvider tokenProvider;

    @Before
    public void voidSetup() {
        ReflectionTestUtils.setField(tokenProvider, "tokenSecret", "secret");
    }

    @Test
    public void testCreateToken() {
        String cardNumber = "123";
        AuthMethod authMethod = AuthMethod.PIN;

        String token = tokenProvider.createToken(cardNumber, true, authMethod);

        assertEquals(cardNumber, tokenProvider.getCardNumberFromToken(token));
        assertTrue(tokenProvider.isAuthenticated(token));
        assertEquals(AuthMethod.PIN, tokenProvider.getAuthMethodFromToken(token));
        assertTrue(tokenProvider.validateToken(token));

    }

}