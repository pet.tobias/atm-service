package com.tobias.atmservice.service;

import com.tobias.atmservice.dto.AuthMethod;
import com.tobias.atmservice.exception.AtmException;
import com.tobias.atmservice.security.TokenProvider;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class TokenVerificationServiceTest {

    @InjectMocks
    private TokenVerificationService tokenVerificationService;

    @Mock
    private TokenProvider tokenProvider;

    @Test
    public void testValidateCardNumber() throws AtmException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        String cardNumber = "123";
        String token = "Bearer token";


        when(request.getHeader("Authorization")).thenReturn(token);
        when(tokenProvider.getCardNumberFromToken("token")).thenReturn(cardNumber);

        tokenVerificationService.validateCardNumber(request, cardNumber);
    }

    @Test(expected = AtmException.class)
    public void testValidateCardNumberInvalid() throws AtmException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        String cardNumber = "123";
        String token = "Bearer token";


        when(request.getHeader("Authorization")).thenReturn(token);
        when(tokenProvider.getCardNumberFromToken("token")).thenReturn("1");

        tokenVerificationService.validateCardNumber(request, cardNumber);
    }

    @Test
    public void testVerifyCorrectAuthMethod() throws AtmException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        AuthMethod authMethod = AuthMethod.PIN;
        String token = "Bearer token";


        when(request.getHeader("Authorization")).thenReturn(token);
        when(tokenProvider.getAuthMethodFromToken("token")).thenReturn(AuthMethod.PIN);

        tokenVerificationService.verifyCorrectAuthMethod(request, authMethod);
    }

    @Test(expected = AtmException.class)
    public void testVerifyCorrectAuthMethodInvalid() throws AtmException {
        HttpServletRequest request = mock(HttpServletRequest.class);
        AuthMethod authMethod = AuthMethod.FINGER_PRINT;
        String token = "Bearer token";


        when(request.getHeader("Authorization")).thenReturn(token);
        when(tokenProvider.getAuthMethodFromToken("token")).thenReturn(AuthMethod.PIN);

        tokenVerificationService.verifyCorrectAuthMethod(request, authMethod);
    }
}