package com.tobias.atmservice.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Pattern;

@Getter
@Setter
public class AbstractRequestDTO {

    @Pattern(regexp = "[0-9]+")
    private String cardNumber;
}
