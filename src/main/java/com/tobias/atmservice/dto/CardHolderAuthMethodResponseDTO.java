package com.tobias.atmservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CardHolderAuthMethodResponseDTO {

    private AuthMethod authMethod;
    private String cardNumber;
    private String token;
}
