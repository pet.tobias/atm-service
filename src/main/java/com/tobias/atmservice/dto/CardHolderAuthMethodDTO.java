package com.tobias.atmservice.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CardHolderAuthMethodDTO extends AbstractRequestDTO {

    private AuthMethod authMethod;
}
