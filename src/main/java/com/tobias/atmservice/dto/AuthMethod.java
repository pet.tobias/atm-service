package com.tobias.atmservice.dto;

public enum AuthMethod {
    FINGER_PRINT,
    PIN
}
