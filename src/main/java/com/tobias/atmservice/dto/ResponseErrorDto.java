package com.tobias.atmservice.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
public class ResponseErrorDto {

    private LocalDateTime timestamp;
    private int status;
    private String message;
}
