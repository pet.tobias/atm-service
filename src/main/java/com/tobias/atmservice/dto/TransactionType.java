package com.tobias.atmservice.dto;

public enum TransactionType {
    WITHDRAWAL,
    DEPOSIT
}
