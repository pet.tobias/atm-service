package com.tobias.atmservice.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptDTO {

    private TransactionType transactionType;
    private BigDecimal amount;
    private BigDecimal newBalance;
}
