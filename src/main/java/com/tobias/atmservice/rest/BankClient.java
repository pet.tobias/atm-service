package com.tobias.atmservice.rest;

import com.tobias.atmservice.dto.*;
import com.tobias.atmservice.exception.AtmException;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;


@RequiredArgsConstructor
@Slf4j
@Service
@CircuitBreaker(name="bankService")
public class BankClient {

    @Value("${bankservice.creds}")
    private String bankCredentials;

    @Value("${bankservice.url}")
    private String bankUrl;

    private final RestTemplate restTemplate;

    public CardHolderAuthMethodDTO validateCardNumber(String cardNumber) throws AtmException {
        String url = bankUrl + "card-holder/" + cardNumber;
        try {
            ResponseEntity<CardHolderAuthMethodDTO> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(getHeader()), CardHolderAuthMethodDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new AtmException(e.getStatusCode(), e.getMessage());
        }
    }

    public void validatePin(CardHolderLoginDTO loginDTO) throws AtmException {
        String url = bankUrl + "card-holder/login";
        try {
            restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(loginDTO, getHeader()), Void.class);
        } catch (HttpClientErrorException e) {
            throw new AtmException(e.getStatusCode(), e.getMessage());
        }
    }

    public ReceiptDTO printReceipt(String cardNumber) throws AtmException {
        String url = bankUrl + "account/receipt/" + cardNumber;
        try {
            ResponseEntity<ReceiptDTO> response = restTemplate.exchange(url, HttpMethod.GET, new HttpEntity<>(getHeader()), ReceiptDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new AtmException(e.getStatusCode(), e.getMessage());
        }
    }

    public AccountBalanceDTO withdrawMoney(TransactionRequestDTO transactionRequestDTO) throws AtmException {
        String url = bankUrl + "account/withdraw";
        try {
            ResponseEntity<AccountBalanceDTO> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(transactionRequestDTO, getHeader()), AccountBalanceDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new AtmException(e.getStatusCode(), e.getMessage());
        }
    }

    public AccountBalanceDTO depositMoney(TransactionRequestDTO transactionRequestDTO) throws AtmException {
        String url = bankUrl + "account/deposit";
        try {
            ResponseEntity<AccountBalanceDTO> response = restTemplate.exchange(url, HttpMethod.POST, new HttpEntity<>(transactionRequestDTO, getHeader()), AccountBalanceDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new AtmException(e.getStatusCode(), e.getMessage());
        }
    }

    public CardHolderAuthMethodDTO updateAuthMethod(CardHolderAuthMethodDTO authMethodRequestDTO) throws AtmException {
        String url = bankUrl + "card-holder/auth-method";
        try {
            ResponseEntity<CardHolderAuthMethodDTO> response = restTemplate.exchange(url, HttpMethod.PUT, new HttpEntity<>(authMethodRequestDTO, getHeader()), CardHolderAuthMethodDTO.class);
            return response.getBody();
        } catch (HttpClientErrorException e) {
            throw new AtmException(e.getStatusCode(), e.getMessage());
        }
    }

    private HttpHeaders getHeader() {
        String encodedCredentials = new String(Base64.encodeBase64(bankCredentials.getBytes()));
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Authorization", "Basic " + encodedCredentials);
        httpHeaders.setAccept(List.of(MediaType.APPLICATION_JSON));
        return httpHeaders;
    }


}
