package com.tobias.atmservice.controller;

import com.tobias.atmservice.dto.AuthMethod;
import com.tobias.atmservice.dto.CardHolderAuthMethodDTO;
import com.tobias.atmservice.dto.CardHolderAuthMethodResponseDTO;
import com.tobias.atmservice.dto.CardHolderLoginDTO;
import com.tobias.atmservice.exception.AtmException;
import com.tobias.atmservice.rest.BankClient;
import com.tobias.atmservice.security.TokenProvider;
import com.tobias.atmservice.service.TokenVerificationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Pattern;

@RequiredArgsConstructor
@RestController
@Validated
@Slf4j
@RequestMapping("/api/validate")
public class AuthController {

	private final TokenProvider tokenProvider;
	private final BankClient bankClient;
	private final TokenVerificationService tokenVerificationService;
	
	@GetMapping("/public/{cardNumber}")
	public CardHolderAuthMethodResponseDTO validateCardNumber(
			@PathVariable
			@Pattern(regexp = "[0-9]+")
			String cardNumber) throws AtmException {
		log.info("card number: {} requested validation", cardNumber);
		CardHolderAuthMethodDTO authMethodDTO = bankClient.validateCardNumber(cardNumber);
		String jwt = tokenProvider.createToken(cardNumber, false, authMethodDTO.getAuthMethod());
		log.info("card number: {} successfully validated", cardNumber);
		return new CardHolderAuthMethodResponseDTO(authMethodDTO.getAuthMethod(), cardNumber, jwt);
	}

	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	@PostMapping("/pin")
	public ResponseEntity<String> validatePin(@RequestBody CardHolderLoginDTO loginDTO,
											  HttpServletRequest request) throws AtmException {
		log.info("card number: {} attempted login via pin", loginDTO.getCardNumber());
		tokenVerificationService.verifyCorrectAuthMethod(request, AuthMethod.PIN);
		bankClient.validatePin(loginDTO);
		String jwt = tokenProvider.createToken(loginDTO.getCardNumber(), true, null);
		log.info("card number: {} successfully logged in", loginDTO.getCardNumber());
		return ResponseEntity.ok(jwt);
	}

	@Operation(security = @SecurityRequirement(name = "bearerAuth"))
	@PostMapping("/fingerprint")
	public ResponseEntity<String> validateFingerprint(@RequestBody CardHolderLoginDTO loginDTO,
												 HttpServletRequest request) throws AtmException {
		log.info("card number: {} attempted login via fingerprint", loginDTO.getCardNumber());
		tokenVerificationService.verifyCorrectAuthMethod(request, AuthMethod.FINGER_PRINT);
		bankClient.validatePin(loginDTO);
		String jwt = tokenProvider.createToken(loginDTO.getCardNumber(), true, null);
		log.info("card number: {} successfully logged in", loginDTO.getCardNumber());
		return ResponseEntity.ok(jwt);
	}

}
