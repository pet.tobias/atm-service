package com.tobias.atmservice.controller;

import com.tobias.atmservice.dto.AccountBalanceDTO;
import com.tobias.atmservice.dto.CardHolderAuthMethodDTO;
import com.tobias.atmservice.dto.ReceiptDTO;
import com.tobias.atmservice.dto.TransactionRequestDTO;
import com.tobias.atmservice.exception.AtmException;
import com.tobias.atmservice.rest.BankClient;
import com.tobias.atmservice.service.TokenVerificationService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.Pattern;

@RequiredArgsConstructor
@RestController
@Slf4j
@SecurityRequirement(name = "bearerAuth")
@RequestMapping("/api/account")
@PreAuthorize("hasRole('ROLE_AUTHENTICATED')")
public class AccountController {

    private final BankClient bankClient;
    private final TokenVerificationService tokenVerificationService;

    @GetMapping("/receipt/{cardNumber}")
    public ReceiptDTO getReceipt(
            @PathVariable @Pattern(regexp = "[0-9]+") String cardNumber,
            HttpServletRequest request) throws AtmException {
        log.info("card number: {} requested receipt", cardNumber);
        tokenVerificationService.validateCardNumber(request, cardNumber);
        return bankClient.printReceipt(cardNumber);
    }

    @PostMapping("/withdraw")
    public AccountBalanceDTO withdraw(
            @RequestBody @Valid TransactionRequestDTO transactionRequestDTO,
            HttpServletRequest request) throws AtmException {
        log.info("card number: {} withdrawing money", transactionRequestDTO.getCardNumber());
        tokenVerificationService.validateCardNumber(request, transactionRequestDTO.getCardNumber());
        AccountBalanceDTO response = bankClient.withdrawMoney(transactionRequestDTO);
        log.info("card number: {} successfully withdrawn money", transactionRequestDTO.getCardNumber());
        return response;
    }

    @PostMapping("/deposit")
    public AccountBalanceDTO deposit(
            @RequestBody @Valid TransactionRequestDTO transactionRequestDTO,
            HttpServletRequest request) throws AtmException {
        log.info("card number: {} depositing money", transactionRequestDTO.getCardNumber());
        tokenVerificationService.validateCardNumber(request, transactionRequestDTO.getCardNumber());
        AccountBalanceDTO response = bankClient.depositMoney(transactionRequestDTO);
        log.info("card number: {} successfully deposited money", transactionRequestDTO.getCardNumber());
        return response;
    }


    @PutMapping("/auth-method")
    public CardHolderAuthMethodDTO updateAuthMethod(
            @Valid @RequestBody CardHolderAuthMethodDTO authMethodDTO,
            HttpServletRequest request)
            throws AtmException {
        log.info("card number: {} updating auth method", authMethodDTO.getCardNumber());
        tokenVerificationService.validateCardNumber(request, authMethodDTO.getCardNumber());
        CardHolderAuthMethodDTO response = bankClient.updateAuthMethod(authMethodDTO);
        log.info("card number: {} updated auth method to {}", authMethodDTO.getCardNumber(),
                authMethodDTO.getAuthMethod());
        return response;
    }
}
