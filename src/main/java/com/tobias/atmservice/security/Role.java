package com.tobias.atmservice.security;

import lombok.Getter;

@Getter
public enum Role {
    ROLE_VERIFIED,
    ROLE_AUTHENTICATED;

}


