package com.tobias.atmservice.security;

import java.util.Date;

import com.tobias.atmservice.dto.AuthMethod;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TokenProvider {
  
	@Value("${token.secret}")
	private String tokenSecret;  
	private static final String AUTHENTICATED = "authenticated";
	private static final String AUTH_METHOD = "auth_method";

	public static final long SHORT_TOKEN_VALIDITY = 300000;
	public static final long LONG_TOKEN_VALIDITY = 3000000;

	public String createToken(String cardNumber, boolean authenticated, AuthMethod authMethod) {
		Date expiryDate = new Date(new Date().getTime() + (authenticated ? LONG_TOKEN_VALIDITY : SHORT_TOKEN_VALIDITY));
		return Jwts.builder()
				.setSubject(cardNumber)
				.claim(AUTHENTICATED, authenticated)
				.claim(AUTH_METHOD, authMethod)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(expiryDate)
				.signWith(SignatureAlgorithm.HS512, tokenSecret)
				.compact();
	}

	public String getCardNumberFromToken(String token) {
		Claims claims = Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(token).getBody();

		return claims.getSubject();
	}

	public boolean isAuthenticated(String token) {
		Claims claims = Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(token).getBody();
		return claims.get(AUTHENTICATED, Boolean.class);
	}

	public AuthMethod getAuthMethodFromToken(String token) {
		final Claims claims = Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(token).getBody();
		String method = claims.get(AUTH_METHOD, String.class);
		return AuthMethod.valueOf(method);
	}

	public boolean validateToken(String authToken) {
		try {
			Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(authToken);
			return true;
		} catch (MalformedJwtException ex) {
			log.error("Invalid JWT token");
		} catch (ExpiredJwtException ex) {
			log.error("Expired JWT token");
		} catch (UnsupportedJwtException ex) {
			log.error("Unsupported JWT token");
		} catch (IllegalArgumentException ex) {
			log.error("JWT claims string is empty.");
		}
		return false;
	} 


}
