package com.tobias.atmservice.exception;

import java.time.LocalDateTime;

import com.tobias.atmservice.dto.ResponseErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = { AtmException.class })
    protected ResponseEntity<ResponseErrorDto> handleDemoAppException(AtmException exception, WebRequest request) {
        log.error("Exception '{}' occurred. Message: {}", exception.getStatus(), exception.getErrorMessage());

        ResponseErrorDto responseErrorDto = new ResponseErrorDto();
        responseErrorDto.setTimestamp(LocalDateTime.now());
        responseErrorDto.setStatus(exception.getStatus().value());
        responseErrorDto.setMessage(exception.getErrorMessage());

        return ResponseEntity.status(exception.getStatus()).body(responseErrorDto);
    }
}
