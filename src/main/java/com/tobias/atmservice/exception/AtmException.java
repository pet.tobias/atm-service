package com.tobias.atmservice.exception;

import java.text.MessageFormat;

import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class AtmException extends Exception {

    private final HttpStatus status;
    private final String errorMessage;

    public AtmException(HttpStatus status, String message) {
        this.status = status;
        this.errorMessage = message;
    }
}
