package com.tobias.atmservice.service;

import com.tobias.atmservice.dto.AuthMethod;
import com.tobias.atmservice.exception.AtmException;
import com.tobias.atmservice.security.TokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@Service
public class TokenVerificationService {


    private static final String AUTHORIZATION = "Authorization";
    private static final String TOKEN_PREFIX = "Bearer ";

    private final TokenProvider tokenProvider;


    public void validateCardNumber(HttpServletRequest request, String cardNumber) throws AtmException {
        if (!tokenProvider.getCardNumberFromToken(getTokenFromRequest(request)).equals(cardNumber)) {
            throw new AtmException(HttpStatus.FORBIDDEN,
                    "Card number is forbidden for this request");
        }
    }

    public void verifyCorrectAuthMethod(HttpServletRequest request, AuthMethod preferredMethod) throws AtmException {
        AuthMethod authMethod = tokenProvider.getAuthMethodFromToken(getTokenFromRequest(request));
        if (!authMethod.equals(preferredMethod)) {
            throw new AtmException(HttpStatus.BAD_REQUEST,
                    "Wrong auth method. Update preferred method or provide correct method");
        }
    }


    private String getTokenFromRequest(HttpServletRequest request) {
        String bearerToken = request.getHeader(AUTHORIZATION);
        return bearerToken.replace(TOKEN_PREFIX, "");
    }
}
