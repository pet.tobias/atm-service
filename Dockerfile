FROM maven:3.6.1-jdk-11 AS packager
ADD . /microservice
RUN \
    cd /microservice && \
    mvn clean install

FROM openjdk:11-jre-slim
COPY --from=packager /microservice /microservice
EXPOSE 8080
CMD java -jar /microservice/target/atm-service-0.0.1-SNAPSHOT.jar
